# Lists

1. note:

    - always use 3 blank spaces for a `nested` list

    - don't leave blank lines between the items

    - always leave a blank lines between the paragraph or heading and the subsequent list, otherwise the list wouldn't render.

    <br>    
1. ordered lists

    ```
    1. item 1

        1. iterm 1
    
        1. iterm 2
    
    1. iterm 2

    1. iterm 3
    ```
    `with output`

    1. item 1

        1. iterm 1
    
        1. iterm 2
    
    1. iterm 2

    1. iterm 3

    <br>
1. unordered lists

    ```
    - term 1

    - term 2

        - term 1

        - term 2

    - term 3

    - term 4
    ```
    `with output`

    - term 1

    - term 2

        - term 1

        - term 2

    - term 3

    - term 4
	
	<br>
1. split lists:

    - [split lists](https://about.gitlab.com/handbook/markdown-guide/#split-lists)
