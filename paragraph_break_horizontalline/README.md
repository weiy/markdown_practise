# paragraphs, breaks, horizontal lines

1.  how to use

    - wrapping text: line won't be broke unless there is a blank line between them.
        ```
        Text A
        Text B
        ```
        `will be`

        ```
        Text A Text B 
        ```

    - addition breaks: using **HTML** style: \<br>
        
        ```
       Text A
       
       Text B
        ```

        `is the same with`

        ```
        Text A

        (no matter how many blank lines here, unless there are <br> here)

        Text B
        ```

    - horizontal line

        ```
        Text A
        <balck line here>
        ---
        <balck line here>
        Text B
        ```
        `output`

        Text A

        ---

        Text B

       
