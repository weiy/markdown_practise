# Header markdown style

1. how to use

    - h1: \#, h2:\##, etc

    - do not use \<h1\>, html header

    - step by step, \## should just be followed by \###

    - always leave a blank space after \#
    
    - should be a line between heading and subsection
