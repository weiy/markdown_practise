# Emphasis: bold and italic

1. Styles

    - bold
     
      `**bold**` output: **bold**

    - italic
     
      `*italic*` output: *italic*

    - bold and italic

        `***bold and italic***` output: ***bold and italic***
