
# Which editor should I choose in centos 

1. **Install vscode from offical website**

   - open with command: code README.md

   - change the hotkey style of vscode as emacs (go to search 
   https://www.google.com) 

   - how to change a specific hotkey: `File` &#8594; `preferences` &#8594; `keyboard shortcuts` &#8594; `search ` : for example: __paste__, __copy__, __preview(open preview to the side)__. and change the hotkey  

   - add `Ctrl-P` to be `preview on the side` by setting hotkey 

   - click top right corner button: **open  preview to the side** 

   - then edit as you want

1. **``(not encouraged)`` Install markdown package *markdown package* in emacs**

   - download link: [markdown in emacs](https://jblevins.org/projects/markdown-mode/)

   - the details can be found in this section [markdonw in emacs](The-following-is-not-encouraged)

# How to use markdown

1. How to use markdown especially in gitlab

   - https://about.gitlab.com/handbook/markdown-guide/

   - new added one: https://gitlab.com/gitlab-org/gitlab/-/blob/master/doc/user/markdown.md


## Standard Markdown commands

1. [header](header)

1. [paragraphs, breaks, horizontal lines](paragraph_break_horizontalline)

1. [emphsis: bold, italic](emphasis_bold_italic)

1. [links](link)

1. [lists](list)

1. [colors](color)

1. for arrow and some other symbols, please use latex format: like

    ```
    $`H{\rightarrow}ZZ{\rightarrow}4l`$
    ```
    - $`H{\rightarrow}ZZ{\rightarrow}4l`$
    

   `The followings haven't been done`

1. [images](img)

1. [videos](video)

1. [tables](table)

1. [diagrams](diagram)

1. [blocks](block)

1. [notes](note)

1. [etc...](etc)

1. blocks that can be collapsible:

<details>
  <summary>Click to expand/collapse</summary>
  <p>This is the hidden content that you can show or hide by clicking the button above.</p>
  <p>Here you can place more text, images, or any other elements supported by Markdown.</p>
</details>


## GitLab new markdonw commands

1. [to be done](to_be_done)
---
<br>

# The following is not encouraged
---

## This is a tutorial on how to configure and use markdown in emacs

### How to configure

1. The final result is: ![markdown live preview](img/markdown_live_view.png "markdown live preview")

1. pip3 install --user grip

1. M-x package-install RET grip-mode

1. M-x grip-mode

1. The details are in [stackoverflow](https://stackoverflow.com/questions/36183071/how-can-i-preview-markdown-in-emacs-in-real-time "How to configure markdown live preview")

### How to use

1. emacs change windows: C-c o (o means: others)

1. insert certain type: M-x markdown-insert- RET 

   1. for example: M-x markdown-insert-link
