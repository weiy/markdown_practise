# This is a practice on blockquotes


1. single line

    - single line

        ```
         > This is a blockquote.
        ```
        output
        >This is a blockquote
    
    <br>

1. multiple lines

    - lines without new blank line will gather together
        ```
        >  On multiple lines.
        That may be lazy.

        ```
        output

        >  On multiple lines.
         That may be lazy.
    
        <br>
    - multiple lines with different paragraph

        ```
        > this is a
        >
        > this is b
        ```
        
        output
        
        > this is a
        >
        > this is b

        <br>
    - `>>>` doesn't support    

1. nested quote

    ```
    > This is nest quote
    >
    >> hello
    >
    > out nested
    ```

    ouput

    > This is nested quote
    >
    >> hello
    > 
    > out nested 