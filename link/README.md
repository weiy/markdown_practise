# Links 

1. notes

    - check for broken links: http://www.deadlinkchecker.com

    <br>
1. inline links

    - inline link format: `[text to display](link)`

    - inline link to be opend in a `new tab` by adding `{:target="_blank"}` to the end: `[text to display](link){:target="_blank"}` 

    <br>

1. relative links

    - the home directory is the current directory. For example, in order to access the `header README.md` under header folder, use link: `[header README.md](../header/README.md)` with output: [header README.md](../header/README.md) 

    <br>

1.  mailto links

    - `mailto` link: `[yingjie.wei@cern.ch](mailto:yingjie.wei@cern.ch)` with output: [yingjie.wei@cern.ch](mailto:yingjie.wei@cern.ch)

    <br> 

1. repeated links

    - [gitlab examples](https://about.gitlab.com/handbook/markdown-guide/#identifiers)
